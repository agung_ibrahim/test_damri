package com.agung.test_damri.deps

import com.agung.test_damri.network.NetworkModule
import com.agung.test_damri.page.MainActivity


import javax.inject.Singleton

import dagger.Component

@Singleton
@Component(modules = [NetworkModule::class])
interface Deps {
    //    void inject(HomeActivity homeActivity);
    fun inject(mainActivity: MainActivity)
}
