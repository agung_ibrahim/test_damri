package com.agung.test_damri.network

import com.agung.test_damri.network.response.ResponseList
import com.agung.test_damri.network.response.ResponseToken
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST
import rx.Observable

interface NetworkService {

    @FormUrlEncoded
    @POST("authtimeout/token")
    fun postToken(@Field("username") username: String,
                  @Field("password") password: String): Observable<ResponseToken>

    @FormUrlEncoded
    @POST("apps/damriapps/getOrigin")
    fun postList (@Header("Authorization") Authorization: String, @Field("test") test: String) : Observable<ResponseList>

}
