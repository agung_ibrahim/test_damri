package com.agung.test_damri.network;

import android.util.Log
import com.agung.test_damri.network.response.ResponseList
import com.agung.test_damri.network.response.ResponseToken
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import kotlin.Exception

class Service(private val networkService: NetworkService) {

    fun getToken(callback: TokenCallback): Subscription {
        return networkService.postToken("test", "damri123")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
            .subscribe(object : Subscriber<ResponseToken>() {
                override fun onCompleted() {

                }

                override fun onError(e: Throwable?) {
                    try {
                        callback.onError(NetworkError(e))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

                override fun onNext(response: ResponseToken) {
                    callback.onSuccess(response)
                }


            })
    }

    fun getListData(token: String, callback: ListDataCallback): Subscription {
        return networkService.postList(token,"")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext{ throwable -> Observable.error(throwable)}
            .subscribe(object : Subscriber<ResponseList>() {
                override fun onNext(response: ResponseList) {
                    callback.onSuccess(response)
                }

                override fun onCompleted() {

                }

                override fun onError(e: Throwable?) {
                    try {
                        callback.onError(NetworkError(e))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })
    }

    interface BaseCallback {
        fun onError(networkError: NetworkError)
    }

    interface TokenCallback : BaseCallback {
        fun onSuccess(response: ResponseToken)
    }

    interface ListDataCallback : BaseCallback {
        fun onSuccess(response: ResponseList)
    }
}

