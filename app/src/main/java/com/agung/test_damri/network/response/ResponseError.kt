package com.agung.test_damri.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseError {
    @Expose
    @SerializedName("status")
    var status: Boolean? = null
    @Expose
    @SerializedName("error")
    var error: String? = null
}