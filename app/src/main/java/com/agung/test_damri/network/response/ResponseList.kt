package com.agung.test_damri.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseList {
    @Expose
    @SerializedName("data")
    var data: List<DataEntity>? = null

    @Expose
    @SerializedName("response_code")
    var responseCode: String? = null

    @Expose
    @SerializedName("status")
    var status: String? = null

    class DataEntity {
        @Expose
        @SerializedName("alamat")
        var alamat: String? = null

        @Expose
        @SerializedName("longloc")
        var longloc: String? = null

        @Expose
        @SerializedName("latloc")
        var latloc: String? = null

        @Expose
        @SerializedName("nm_kota")
        var nmKota: String? = null

        @Expose
        @SerializedName("nm_asal")
        var nmAsal: String? = null

        @Expose
        @SerializedName("asal")
        var asal: String? = null

    }
}