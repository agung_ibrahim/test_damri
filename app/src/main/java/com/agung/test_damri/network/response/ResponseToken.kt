package com.agung.test_damri.network.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseToken {
    @Expose
    @SerializedName("token")
    var token: String? = null
}