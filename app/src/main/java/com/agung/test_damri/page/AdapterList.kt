package com.agung.test_damri.page

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agung.test_damri.R
import com.agung.test_damri.network.response.ResponseList
import kotlinx.android.synthetic.main.item_recycleviewlist_layout.view.*

class AdapterList (private val context: Context) : RecyclerView.Adapter<AdapterList.ItemHolder>() {

    private val listDataRute : ArrayList<ResponseList.DataEntity> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_recycleviewlist_layout, viewGroup, false))
    }

    override fun getItemCount(): Int = listDataRute.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item = listDataRute[position]
        if (item.nmAsal != null) holder.textKota.text = item.nmKota
        if (item.nmKota != null) holder.textTerminal.text = item.nmAsal
    }

    inner class ItemHolder(v: View): RecyclerView.ViewHolder(v) {
        var textKota = v.txtKota
        var textTerminal = v.txtTerminal
    }

    fun updateList(listItem: List<ResponseList.DataEntity>) {
        listDataRute.clear()
        listDataRute.addAll(listItem)
        notifyDataSetChanged()

    }
}