package com.agung.test_damri.page

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.agung.test_damri.R
import com.agung.test_damri.network.Service
import com.agung.test_damri.network.response.ResponseList
import com.agung.test_damri.network.response.ResponseToken
import com.agung.test_damri.utils.BaseActivityApp
import com.google.gson.Gson
import kotlinx.android.synthetic.main.main_layout.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : BaseActivityApp(), MainActivityView {

    @Inject
    lateinit var service: Service
    lateinit var presenter: MainActivityPresenter
    private var mAdapterList : AdapterList? = null
    val listData : ArrayList<ResponseList.DataEntity> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        init()
    }

    fun init() {
        deps?.inject(this)
        presenter = MainActivityPresenter(service, this)

        mAdapterList = AdapterList(this)
        var layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvList.layoutManager = layoutManager
        rvList.adapter = mAdapterList

        presenter.postToken()

        searchText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().count() > 0) {
                    onSearch(p0.toString())
                } else {
                    mAdapterList?.updateList(listData)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
    }

    override fun onSuccessToken(response: ResponseToken) {
        if (response.token != null) {
            presenter.getList(response.token.toString())
        } else {
            toast("gagal mendapatkan token")
        }
    }

    override fun onErrorToken(response: String) {
        toast(response)
    }

    override fun onSuccessGetList(response: ResponseList) {
//        toast("Sukses dapet list")
        Timber.d("data ${Gson().toJson(response.data)}")
        if (response.data != null) {
            listData.clear()
            listData.addAll(response.data!!)
            mAdapterList?.updateList(listData)
        } else {
            toast("data list kosong")
        }
    }

    override fun onErrorGetList(response: String) {
        toast("Get List Gagal")
    }

    override fun onShowLoading() {
        showDialog(true)
    }

    override fun onHideLoading() {
        showDialog(false)
    }

    fun onSearch(textKota: String) {
        if (!textKota.equals(null) && listData.count() != null) {
            val result = listData.filter {
                it.nmKota!!.contains(textKota, ignoreCase = true)
            }

            val gson = Gson()
            val jsonString = gson.toJson(result)
            Log.e("dataFilter", jsonString)
            mAdapterList?.updateList(result)
        }
    }

    fun toast(text: String) {
        Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
    }
}