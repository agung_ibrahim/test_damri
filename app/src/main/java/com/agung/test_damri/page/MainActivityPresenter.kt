package com.agung.test_damri.page

import com.agung.test_damri.network.NetworkError
import com.agung.test_damri.network.Service
import com.agung.test_damri.network.response.ResponseList
import com.agung.test_damri.network.response.ResponseToken
import rx.subscriptions.CompositeSubscription

class MainActivityPresenter (internal var service: Service, internal var view: MainActivityView){
    internal var subscriptions: CompositeSubscription

    init {
        this.subscriptions = CompositeSubscription()
    }

    fun postToken() {
        view.onShowLoading()
        val subscription = service.getToken(object : Service.TokenCallback {
            override fun onSuccess(response: ResponseToken) {
                view.onHideLoading()
                view.onSuccessToken(response)
            }

            override fun onError(networkError: NetworkError) {
                view.onHideLoading()
                view.onErrorToken(networkError.appErrorMessage)
            }

        })

        subscriptions.add(subscription)
    }

    fun getList(token : String) {
        view.onShowLoading()
        val subscription = service.getListData(token, object : Service.ListDataCallback {
            override fun onSuccess(response: ResponseList) {
                view.onHideLoading()
                view.onSuccessGetList(response)
            }

            override fun onError(networkError: NetworkError) {
                view.onHideLoading()
                view.onErrorGetList(networkError.appErrorMessage)
            }

        })

        subscriptions.add(subscription)
    }

    fun onStop() {
        subscriptions.unsubscribe()
    }
}