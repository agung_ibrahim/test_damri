package com.agung.test_damri.page

import com.agung.test_damri.network.response.ResponseList
import com.agung.test_damri.network.response.ResponseToken

interface MainActivityView {

    fun onSuccessToken(response: ResponseToken)

    fun onErrorToken(response: String)

    fun onSuccessGetList(response: ResponseList)

    fun onErrorGetList(response: String)

    fun onShowLoading()

    fun onHideLoading()
}