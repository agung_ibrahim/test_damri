package com.agung.test_damri.utils

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.agung.test_damri.BuildConfig
import com.agung.test_damri.R
import com.agung.test_damri.deps.DaggerDeps
import com.agung.test_damri.deps.Deps
import com.agung.test_damri.network.NetworkModule
import timber.log.Timber

import java.io.File

/**
 * Created by ennur on 6/28/16.
 */
open class BaseActivityApp : AppCompatActivity() {
    internal var deps: Deps? = null

    internal var pd: LoadingDialog? = null
    lateinit var fm: FragmentManager
    lateinit var builder: AlertDialog.Builder

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        builder = AlertDialog.Builder(this)
        pd = LoadingDialog(this)
        val cacheFile = File(cacheDir, "responses")
        deps = DaggerDeps.builder().networkModule(NetworkModule(cacheFile)).build()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }

    fun getDeps(): Deps {
        return deps!!
    }

    protected fun loadFragment(fragment: Fragment, baseframe: View) {
        val ft = fm.beginTransaction()
        ft.add(baseframe.id, fragment)
        ft.commit()
    }

    protected fun replaceFragment(fragment: Fragment, baseframe: View) {
        val ft = fm.beginTransaction()
        ft.replace(baseframe.id, fragment)
        ft.addToBackStack("")
        ft.commit()
    }

    fun showDialog(show: Boolean) {
        if (null != pd)
            if (show)
                pd!!.show()
            else
                pd!!.dismiss()
    }

    fun showMessage(message: String) {
        builder.setMessage(message)
                .setTitle(getString(R.string.app_name))
                .setPositiveButton("OK") { dialogInterface, i -> dialogInterface.dismiss() }
                .show()
    }


}
